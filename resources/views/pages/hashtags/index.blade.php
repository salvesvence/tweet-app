@extends('templates.default')

@section('content')

    <div class="row">
        <div class="col-12">
            @if($hashtags->count() > 0)
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Hashtag</th>
                        <th scope="col">Visualizar Gráfica</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($hashtags as $hashtag)
                        <tr>
                            <th scope="row">{{ $hashtag->id }}</th>
                            <td>{{ $hashtag->content }}</td>
                            <td>
                                <a href="{{ route('hashtags.show', $hashtag->id) }}">VISUALIZAR</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>No hay hashtags.</p>
            @endif
        </div>
    </div>

@endsection