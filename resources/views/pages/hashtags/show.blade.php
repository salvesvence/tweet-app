@extends('templates.default')

@section('content')

    <div class="d-none" id="data-element" data-hashtag="{{ $hashtag }}"
         data-tweets="{{ $hashtag->tweets }}">
    </div>

    <div class="row">
        <div class="chart-container col-12 col-lg-6">
            <canvas id="hour-chart"></canvas>
        </div>
        <div class="chart-container col-12 col-lg-6">
            <canvas id="day-chart"></canvas>
        </div>
    </div>

@endsection