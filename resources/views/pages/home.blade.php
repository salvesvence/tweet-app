@extends('templates.default')

@section('content')

    <div class="flex-center position-ref full-height">
        <div class="content">

            <form action="{{ route('twitters.store') }}" method="post" class="form-inline">
                {{ csrf_field() }}
                <div class="form-group mx-sm-3 mb-2">
                    <label for="hashtag" class="sr-only">Password</label>
                    <input type="text" name="hashtag" class="form-control"
                           id="hashtag" placeholder="Ej: #farina">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Buscar</button>
            </form>

        </div>
    </div>

@endsection