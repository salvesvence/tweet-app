<!doctype html>

<html lang="{{ app()->getLocale() }}">

@include('templates.partials.head')

<body>

    @include('templates.partials.navbar')

    <div class="container">
        @yield('content')
    </div>

    <script src="{{ asset('js/app.js') }}"></script>

</body>

</html>
