var moment = require('moment');

var data = document.getElementById("data-element"),
    hourCtx = document.getElementById("hour-chart").getContext('2d'),
    dayCtx = document.getElementById("day-chart").getContext('2d'),
    hashtag = jQuery.parseJSON(data.dataset.hashtag),
    tweets = jQuery.parseJSON(data.dataset.tweets);

var morning = 0,
    afternoon = 0,
    days = [];

tweets.forEach(function (tweet) {

    var today = new Date(tweet.origin_date);
    var hour = today.getHours();

    days[0] = moment(today);

    for(var i = 1; i < 7; i++) {
        today.setDate(today.getDay() - i);
        days[i] = moment().subtract(i, 'days').calendar();
    }

    if(hour >= 0 && hour <= 12) {
        morning = morning + 1;
    } else {
        afternoon = afternoon + 1;
    }

});

window.onload = function () {

    window.myBar = new Chart(hourCtx, {
        type: 'bar',
        data: {
            labels: ["Entre las 00:00h y las 12:00h", "Entre las 12:00h y las 00:00h"],
            datasets: [{
                label: 'Horas en las que se han escrito tweets con el hashtag ' + hashtag.content,
                data: [morning, afternoon],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)'
                ],
                borderWidth: 1
            }]
        },
        responsive: true,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    window.myRadar = new Chart(dayCtx, {
        type: 'radar',
        data: {
            labels: days,
            datasets: [{
                label: 'Días en los que se han escrito tweets con el hashtag ' + hashtag.content,
                data: [morning, afternoon, morning, afternoon, morning, afternoon, afternoon],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 85, 65, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 85, 65, 1)'
                ],
                borderWidth: 1
            }]
        },
        responsive: true,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
};