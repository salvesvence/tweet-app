# TWEET APP

* Pequeña webapp que permite guardar y visualizar los últimos 100 tweets asociados a un determinado hashtag.

## Instalación

* Necesitaremos tener instalado apache, mysql, php7.1.3 y composer en nuestro sistema operativo al igual que con cualquier proyecto de Laravel 5.6. Una vez instalado deberemos lanzar el siquiente comando para instalar nuestras dependencias:

```
composer install
```
### Base de datos
* Una vez hecho ésto crearemos una base de datos en MySQL, y deberemos añadir los datos de acceso al archivo .env que hay en la raíz del proyecto, ahora mismo tiene los datos siguientes (si queréis podéis utilizar los mismos, solo tienen que coincidir con las credenciales de vuestra base de datos):

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=twitter_app
DB_USERNAME=root
DB_PASSWORD=root
```
* Una vez creada la base de datos con las credenciales correctas, deberemos ejecutar el siguiente comando para así crear las tablas "hashtags", "tweets" y "hashtag_tweet".

```
php artisan migrate
```
* Si queremos borrar y volver a crear las tablas solo tendremos que ejecutar el siguiente comando:
```
php artisan migrate:refresh
```

### Twitter API
* Las credenciales para utilizar la API de Twitter obviamente no las cambiéis, al menos claro que pongáis las vuestras de manera correcta, en mi archivo .env tengo las siguientes:

```
TWITTER_CLIENT_ID=jvCEW8rQi6O4KaJXGLYCJ2ffv
TWITTER_SECRET=v7hRtvHD8ZkzmML0ebzFH95nEFTOvECoDMKWB7IEOXMP2kny3h
TWITTER_TOKEN=1003739803236687872-b17pPNcTyHMa3UhNtR1g4QoszM1pyP
TWITTER_SECRET_TOKEN=YaXhLiv8Cr9cB2riVz8hJR2EteIv9AgwEPXEPAc1WNy5d
```

### Pequeña explicación de su funcionamiento.
* Una vez hecho ésto solo tendremos que lanzar el comando siguiente para levantar el proyecto:

```
php artisan serve
```
* Y en unos segundos justo después de ejecutar éste comando podremos ir a la barra de navegador e insertar la siguiente url:
```
http://localhost:8000/
```
* Y podremos ver la vista principal, en la que veremos un pequeño buscador en el que escribir el hashtag por el cual querramos buscar tweets.
* Por defecto visualizará y guardará los últimos 100 tweets de la última semana asociados al hashtag #farina, pero os animos a que busquéis otros hashtags ;)
* Una vez le demos a buscar, la aplicación buscará en la API de Twitter los tweets mencionados y los guardará automáticamente en nuestra base de datos, para finalmente redirigirnos a la vista que nos enseñará las gráficas de esos tweets.
* Cuando estemos en ésta última vista podremos visualizar 2 gráficas, una de ellas nos muestra cuantos de esos tweets se han escrito por la madrugada hasta el mediodía y cuáles se han escrito desde el mediodía hasta las 00.00h.
* La segunda gráfica nos mostrará en qué días de los últimos 7 se han escrito los tweets.
* A partir de aquí, una vez guardados los primeros tweets, si nos situamos en el menú superior y pinchamos en "Listado Hashtags", ya podremos visitar la url donde se visualizará una tabla con todos los hashtags guardados, y podremos volver a visualizar las gráficas de los tweets asociadas a ellos.

## Puntos de Interés

* Como podréis ver, la parte backend del proyecto está hecho con LARAVEL 5.6.
* La parte frontend está realizada con ECMASCRIPT 6 junto con la librería Chart.js y podéis ver el código si entráis en la carpeta resources/assets/js.
* La maquetación es muy básica y podréis ver que está realizada con SCSS, podéis verlos ingresando en la carpeta resources/assets/sass.

## Autor

* **SILVANO ALVES VENCE** - - [GITHUB](https://github.com/salvesvence)