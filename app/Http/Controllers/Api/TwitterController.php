<?php

namespace App\Http\Controllers\Api;

use App\Hashtag;
use App\Tweet;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

class TwitterController extends Controller
{
    /**
     * @var Client
     */
    private $client;

    /**
     * TwitterController constructor.
     */
    public function __construct()
    {
        $stack = HandlerStack::create();

        $middleware = new Oauth1([
            'consumer_key'    => env('TWITTER_CLIENT_ID'),
            'consumer_secret' => env('TWITTER_SECRET'),
            'token'           => env('TWITTER_TOKEN'),
            'token_secret'    => env('TWITTER_SECRET_TOKEN')
        ]);

        $stack->push($middleware);

        $this->client = new Client([
            'base_uri' => 'https://api.twitter.com/1.1/',
            'handler' => $stack,
            'auth' => 'oauth',
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $existingHashtag = Hashtag::where('content', $this->hashtag($request))->first();

        if(is_null($existingHashtag)) {

            $response = $this->client->get('search/tweets.json', ['query' => [
                'include_entities' => true,
                'count' => '100',
                'until' => Carbon::now()->subDays(7)->toDateString(),
                'q' => $this->hashtag($request)
            ]]);

            $response = json_decode($response->getBody())->statuses;

            $hashtag = Hashtag::create(['content' => $this->hashtag($request)]);

            foreach ($response as $tweet) {

                $origin_date = new Carbon($tweet->created_at);

                $hashtag->tweets()->save(
                    Tweet::create([
                        'text' => $tweet->text,
                        'origin_date' => $origin_date
                    ])
                );
            }

            return redirect()->route('hashtags.show', [
                'hashtag' => $hashtag->id
            ]);
        }

        return redirect()->route('hashtags.show', [
            'hashtag' => $existingHashtag->id
        ]);
    }

    /**
     * Get the hashtag by the request given
     *
     * @param Request $request
     * @return string
     */
    private function hashtag(Request $request)
    {
        $hashtag = $request->hashtag ?: '#farina';

        if( !starts_with($hashtag, '#') ) {
            return "#$hashtag";
        }

        return $hashtag;
    }
}
