<?php

namespace App\Http\Controllers;

use App\Hashtag;
use Illuminate\Http\Request;

class HashtagsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $hashtags = Hashtag::all();

        return view('pages.hashtags.index', compact('hashtags'));
    }

    /**
     * @param $hashtag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Hashtag $hashtag)
    {
        return view('pages.hashtags.show', compact('hashtag'));
    }
}
