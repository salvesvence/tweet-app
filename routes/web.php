<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Pages\HomeController@index');

Route::get('/hashtags', 'HashtagsController@index')
    ->name('hashtags.index');

Route::get('/hashtags/{hashtag}/tweets', 'HashtagsController@show')
    ->name('hashtags.show');

Route::post('/twitter', 'Api\TwitterController@store')
    ->name('twitters.store');

