<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tweets', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text');
            $table->timestamp('origin_date');
            $table->timestamps();
        });

        Schema::create('hashtag_tweet', function (Blueprint $table) {
            $table->integer('hashtag_id')->unsigned();
            $table->integer('tweet_id')->unsigned();

            $table->foreign('hashtag_id')
                ->references('id')
                ->on('hashtags')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('tweet_id')
                ->references('id')
                ->on('tweets')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->primary(['hashtag_id', 'tweet_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hashtag_tweet');
        Schema::dropIfExists('tweets');
    }
}
